package dotsandboxes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class DotsAndBoxesGridTest {
    // Logger for logging test information
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    // Dummy test to ensure the test suite runs correctly
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // Test for the first bug: checking if a square is complete
    @Test
    public void testSquareCompletion() {
        // Create an instance of the DotsAndBoxesGrid class with appropriate grid size
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);
        
        // Set up the grid with three sides drawn
        grid.drawHorizontal(0, 0, 1);
        grid.drawVertical(0, 0, 1);
        grid.drawHorizontal(0, 1, 1);
        
        // Check if the square is complete (it should not be complete with only three sides)
        assertFalse(grid.boxComplete(0, 0));
        
        // Draw the last side to complete the square
        grid.drawVertical(1, 0, 1);
        
        // Now the square should be complete
        assertTrue(grid.boxComplete(0, 0));
    }

    // Test for the second bug: checking for already drawn lines
    @Test
    public void testAlreadyDrawnLine() {
        // Create an instance of the DotsAndBoxesGrid class with appropriate grid size
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);
        
        // Draw a horizontal line
        grid.drawHorizontal(0, 0, 1);
        
        // Attempting to draw the same line again should throw an IllegalStateException
        assertThrows(IllegalStateException.class, () -> {
            grid.drawHorizontal(0, 0, 1);
        });
    }
}
